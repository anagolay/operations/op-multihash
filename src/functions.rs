// This allow us to fully qualify the output type
use crate as op_multihash;

use crate::U64MultihashWrapper;
#[cfg(not(feature = "std"))]
use alloc::{
    collections::BTreeMap,
    format,
    string::{String, ToString},
};
use an_operation_support::{describe, *};
use multihash::{Code, MultihashDigest};

#[cfg(feature = "std")]
use std::collections::BTreeMap;

/// Resolves the hasher string to a [`multihash::Code`]
/// format. Possible options are:
///
/// - sha2_256
/// - blake3_256
///
/// # Arguments
/// - hasher: the hasher string
///
/// # Return
/// the hasher code
pub fn resolve_hasher(hasher: &str) -> Result<Code, String> {
    match hasher {
        #[cfg(feature = "config_hasher_sha2_256")]
        "sha2_256" => Ok(Code::Sha2_256),
        #[cfg(feature = "config_hasher_blake3_256")]
        "blake3_256" => Ok(Code::Blake3_256),
        _ => Err("Invalid hasher configuration".to_string()),
    }
}

/// op_multihash
///
/// Produces a multihash of the [`Bytes`] passed as input using the hashing algorithm indicated
/// in the config.
///
/// The `describe` attribute allow generation of manifest
/// with the following explicit properties:
///
///  * _group_: SYS
///  * _config_:
///    - hasher: choses the hashing algorithm. Allowed values are sha2_256, blake3_256
///  * _features_:
///    - config_hasher: conditional compilation of hasher features
///    - std: support std lib as well as nostd
///
/// # Arguments
///  * bytes: the bytes to hash
///  * config: Operation configuration
///
/// # Return
/// A [`Result`] having an Ok value of [`op_multihash::U64MultihashWrapper`] type and a Err value of
/// [`String`] type.
///
/// **NOTE:** The function returns asynchronously, by mean of a `Future`.
#[describe([
  groups = [
    "SYS",
  ],
  config = [
    hasher = ["sha2_256", "blake3_256"],
  ],
  features = [
    "config_hasher",
    "std"
  ]
])]
pub async fn execute(
    bytes: &Bytes,
    config: BTreeMap<String, String>,
) -> Result<op_multihash::U64MultihashWrapper, String> {
    let hasher = resolve_hasher(config.get("hasher").unwrap_or(&"".to_string()))?;
    let multihash = hasher.digest(bytes.as_slice());
    Ok(U64MultihashWrapper::wrap(multihash))
}

#[cfg(test)]
mod tests {
    use crate::execute;
    use multihash::Code;

    #[test]
    fn test_describe() {
        use an_operation_support::operation::OperationManifestData;
        use serde_json;

        let manifest_data: OperationManifestData =
            serde_json::from_str(&crate::describe()).unwrap();

        assert_eq!("op_multihash", manifest_data.name);
        assert_eq!(1, manifest_data.inputs.len());
        assert_eq!("Bytes", manifest_data.inputs.get(0).unwrap());
        assert_eq!(1, manifest_data.config.len());
        let config_keys: Vec<String> = manifest_data.config.keys().cloned().collect();
        assert_eq!("hasher", config_keys.get(0).unwrap());
        assert_eq!(2, manifest_data.config.get("hasher").unwrap().len());
        assert_eq!(
            &["sha2_256".to_string(), "blake3_256".to_string()],
            manifest_data.config.get("hasher").unwrap().as_slice()
        );
        assert_eq!("op_multihash::U64MultihashWrapper", manifest_data.output);
        assert_eq!("config_hasher", manifest_data.features.get(0).unwrap());
        assert_eq!("std", manifest_data.features.get(1).unwrap());
    }

    #[tokio::test]
    async fn test_execute() {
        let input = b"example".to_vec();
        let config = std::collections::BTreeMap::from_iter([(
            "hasher".to_string(),
            "blake3_256".to_string(),
        )]);
        let op_output = execute(&input, config).await.unwrap();

        assert_eq!(u64::from(Code::Blake3_256), op_output.get_code());
        assert_eq!(
            &vec![
                151, 150, 84, 108, 100, 171, 21, 171, 116, 104, 180, 121, 243, 179, 194, 13, 88,
                64, 175, 5, 172, 15, 153, 154, 215, 160, 137, 81, 45, 1, 87, 46
            ],
            op_output.get_digest()
        );
    }
}
